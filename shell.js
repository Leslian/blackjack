console.log(mdc);

const MDCTopAppBar = mdc.topAppBar.MDCTopAppBar; 
const topAppBarElement = document.querySelector(".mdc-top-app-bar");
const topAppBar = new MDCTopAppBar(topAppBarElement);

const MDCTextFields = mdc.textField.MDCTextField;
const foosrename = [].map.call(
    document.querySelectorAll(".mdc-text-field"),
    function (el) {
        return new MDCTextFields(el);
    }
);