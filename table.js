const MDCTextField =mdc.textField.MDCTextField;
const foos = [].map.call(
    document.querySelectorAll(".mdc-text-field"),
    function (el) {
        return new MDCTextField(el);
    }
);

const suits = ["SPADES", "HEARTS", "DIAMONDS", "CLUBS", ""];
const ranks = ["ACE", 2, 3, 4, 5, 6, 7, 8, 9, 10, "JACK", "QUEEN", "KING"];

let deckId;


function getShoe(callback) {
    fetch("https://www.deckofcardsapi.com/api/deck/new/shuffle?deck_count=6")
        .then((res) =>res.json())
        .then((data) => {
            callback(data);
        });
}

getShoe((data) => (
    deckId = data.deck_id
));

let wager;

function drawFourCards(callback) {
    fetch (`https://www.deckofcardsapi.com/api/deck/${deckId}/draw?count=4`)
        .then((res) => res.json())
        .then((data) => {
            callback(data.cards);
        });
}
function drawOneCard(callback) {
    return fetch (`https://www.deckofcardsapi.com/api/deck/${deckId}/draw?count=1`)
        .then((res) => res.json())
        .then((data) => {
            callback(data.cards[0]);
        });
}

const deck = [];
for (const suit of suits) {
    for (const rank of ranks) {
        deck.push({
            suit,
            rank,
        });
    }
}

function getDeck() {
    return deck;
}
function getRandomCard() {
    return deck[Math.floor(Math.random() * deck.length)];
}

const playersCardList =document.querySelector("#playersCards ol");
const dealersCardList =document.querySelector("#dealersCards ol");

const playersActionsSection = document.querySelector("#playersActions");
const bettingSection = document.querySelector("#betting");
const bettingForm = document.forms[0];
const bankrollSpan = document.querySelector("#player-bankroll");
const wagerInput = bettingForm[0];
const wagerButton = bettingForm[1];
wagerButton.addEventListener("click", makeWager);

const hitButton = document.querySelector("#hit-button");
hitButton.addEventListener("click", hitPlayer);
const standButton = document.querySelector("#stand-button");
standButton.addEventListener("click", dealersTurn);

function dealToDisplay(card) {
    const newCard = document.createElement("li");
    newCard.setAttribute("data-blackjack-value", rankToValue(card.rank));
    newCard.innerText = `${rankToWord(card.rank)} of ${suitToWord(card.suit)}`;

    const playersCardList = document.querySelector("#playerCards ol");
    playersCardList.appendChild(newCard);
}



const mapRanksToWords = {
    2: "Two",
    3: "Three",
    4: "Four",
    5: "Five",
    6: "Six",
    7: "Seven",
    8: "Eight",
    9: "Nine",
    10: "Ten",
    JACK: "Jack",
    QUEEN: "Queen",
    KING: "King",
    ACE: "Ace",
};

function rankToWord(rank) {
    return mapRanksToWords[rank];
}

const mapSuitsToWords = {
    SPADES: "Spades",
    HEARTS: "Hearts",
    DIAMONDS: "Diamonds",
    CLUBS: "Clubs",
    "": "Mystery",
};

function suitToWord(suit) {
    return mapSuitsToWords[suit];
};

const mapRanksToValues ={
    ACE: "11/1",
    KING: "10",
    QUEEN: "10",
    JACK: "10",
    "Face Down": "?",
};

function rankToValue(rank) {
    if (rank in mapRanksToValues) {
        return mapRanksToValues[rank];
    } else {
        return rank.toString();
    }
};


function dealToDisplay(card) {
    const newCard = document.createElement("li");
    newCard.setAttribute("data-blackjack-value", rankToValue(card.rank));
    newCard.innerText = `${rankToWord(card.rank)} of ${suitToWord(card.suit)}`;
    
    const playersCardList = document.querySelector("#playersCards ol")
    playersCardList.appendChild(newCard);
}

let playerBankroll = parseInt(localStorage.getItem("bankroll")) || 2022;
function getBankroll() {
    return playerBankroll;
}

function setBankroll(newBalance) {
    playerBankroll = newBalance;
    localStorage.setItem("bankroll", playerBankroll)
}

function makeWager(e) {
    e.preventDefault();
    //console.log(wagerInput.value);
    wager = parseInt(wagerInput.value);
    timeToPlay();
}

function timeToBet() {
    clearCards();
    playersActionsSection.classList.add("hidden");
    bettingSection.classList.remove("hidden");
    bankrollSpan.innerText = `Bankroll: $${getBankroll()}`;
}

function timeToPlay() {
    clearCards();
    bettingSection.classList.add("hidden");
    playersActionsSection.classList.remove("hidden");
    drawFourCards(dealFourCards);
}

function dealFourCards(fourCards) {
    //console.log("You called me");
    const [first, second, third, fourth] = fourCards;
    dealCard(first);
    dealCard(second, false, false);
    dealCard(third);
    dealCard(fourth, false);
}
const backOfCardImageSrc = "https://previews.123rf.com/images/rlmf/rlmf1512/rlmf151200171/49319432-playing-cards-back.jpg";

let dealersDownCard;

function dealCard(card, isToPlayer = true, isFaceUp = true) {
    const newCard = document.createElement("li");
    const image = document.createElement("img");
    image.setAttribute('src', isFaceUp ? card.image : backOfCardImageSrc);
    if (!isFaceUp) dealersDownCard = card;
    image.setAttribute(
        "alt", 
        isFaceUp 
        ? `${rankToWord(card.value)} of ${suitToWord(card.suit)}`
        : "Face down"
    );
    image.style.height = `210px`;
    image.style.width = `150px`;
    newCard.setAttribute(
        "data-blackjack-value", 
        rankToValue(isFaceUp ? card.value : "Face Down")
    );
    newCard.appendChild(image);
        (isToPlayer ? playersCardList : dealersCardList).appendChild(newCard);
}

function flipDownCard() {
    const downCard = dealersCardList.children[0].children[0];
    downCard.setAttribute("src", dealersDownCard.image);
    downCard.setAttribute(
        "alt",
        `${rankToWord(dealersDownCard.value)} of ${suitToWord(dealersDownCard.value)}`
    );
    downCard.setAttribute(
        "data-blackjack-value",
        rankToValue(dealersDownCard.value)
    )
}

function removeChildren(domNode) {
    while(domNode.firstChild) {
        domNode.removeChild(domNode.firstChild);
    }
}

function clearCards() {
    removeChildren(dealersCardList);
    removeChildren(playersCardList);
}
//saving the total of the player//
function getPlayerTotal(getDealerTotal=false) {
    const playersCards = getDealerTotal
        ? dealersCardList.children
        : playersCardList.children;
    let total = 0; // where the score is starting//
    let aceCount = 0;
    for (const card of playersCards) {
        console.log(card);
        if (card.dataset.blackjackValue == "?") {
            total += parseInt(rankToValue(dealersDownCard.value));
        } else if (card.dataset.blackjackValue == "11/1") {
            total += 11;
            aceCount++;
        } else {
            total += parseInt(card.dataset["blackJackValue"]); 
        }
    }
    if (total > 21) { //incrementing total// 
        while (aceCount > 0) {
            total -= 10;
            aceCount--;
        }
    }
    return total;
}
//total of the dealer total//
function getDealerTotal() {
    return getPlayerTotal(true);
}
// might be wrong (rules for how dealer plays//
async function dealersTurn() { //added async//
    flipDownCard();
    while (getDealerTotal() < 20) {
            await hitDealer(); //added await//
        }
        if (getDealerTotal > 21) {
            console.assertlog("dealer busted");
            takeStakes(true);
        if (getDealerTotal == 16)
        } else {
            evaluateWinner();
        }
}

function evaluateWinner() { //determining who won//
    if (getPlayerTotal() > getDealerTotal ()) {
        console.log("player won");
        takeStakes(true);
    } else if (getPlayerTotal() === getDealerTotal()) {
        console.log("push");
        takeStakes(false, true);
    } else {
        console.log("dealer won");
        takeStakes(false);
    }
}
function hitPlayer(){
    drawOneCard ((card) => {
        dealCard(card);
        if (getPlayerTotal() > 21) bustPlayer();
    });
}
function bustPlayer() {
    console.log("player busted");
    takeStakes(false);
}
function hitDealer() {
    return drawOneCard((card) => dealCard(card, false));
}

function takeStakes(playerWon, wasPush = false, wasANatural = false) {
    if (!wasPush) {
        setBankroll(
            getBankroll() +
            2
        )
    }
    setTimeout(timeToBet, 3000);
}